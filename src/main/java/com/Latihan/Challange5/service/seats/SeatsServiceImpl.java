package com.Latihan.Challange5.service.seats;

import com.erick.challenge4.model.Schedules;
import com.erick.challenge4.model.Seats;
import com.erick.challenge4.repository.SeatsRepository;
import com.erick.challenge4.service.schedules.SchedulesService;
import com.erick.challenge4.service.seats.SeatsService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class SeatsServiceImpl implements SeatsService {

    private SeatsRepository seatsRepository;
    private SchedulesService schedulesService;

    @Override
    public void addSeats(Character stdName, Integer nmrKursi, Long scheduleId) {
        Seats seats =  new Seats();
        Schedules schedules = schedulesService.findSchedulesById(scheduleId);
        seats.setStdName(stdName);
        seats.setNmrKursi(nmrKursi);
        seats.setScheduleId(schedules);
        seatsRepository.save(seats);
    }
}

package com.Latihan.Challange5.service.invoice;

import com.erick.challenge4.model.Films;
import com.erick.challenge4.model.Schedules;
import com.erick.challenge4.model.Users;
import com.erick.challenge4.service.films.FilmsService;
import com.erick.challenge4.service.schedules.SchedulesService;
import com.erick.challenge4.service.users.UsersService;
import lombok.AllArgsConstructor;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@AllArgsConstructor
@Service

public class InvoiceService {

    private UsersService usersService;

    private FilmsService filmsService;

    private SchedulesService schedulesService;

    private HttpServletResponse httpResponse;


    public void getInvoice(String username, Character stdName, Integer nmrKursi, Long scheduleId) throws IOException,JRException{
        JasperPrint jasperPrint = JasperFillManager.fillReport(giveJaspertReport(),giveParameter(),giveBeanCollection(username, stdName, nmrKursi, scheduleId));
        servletResponse();
        JasperExportManager.exportReportToPdfStream(jasperPrint,httpResponse.getOutputStream());
    }

    private List<Map<String, String>> listDataForInvoice(String username, Character stdName, Integer nmrKursi, Long scheduleId) {
        List<Map<String, String>> dataInvoice = new ArrayList<>();
        Map<String, String> data = new HashMap<>();

        Users users = usersService.findUsersByUsername(username);
        Schedules schedules= schedulesService.findSchedulesById(scheduleId);
        data.put("filmName",schedules.getFilmId().getFilmName());
        data.put("stdName",stdName.toString());
        data.put("nmrKursi",nmrKursi.toString());
        data.put("tglTayang",schedules.getTglTayang().toString());
        data.put("jamMulai",schedules.getJamMulai().toString());
        data.put("jamSelesai",schedules.getJamSelesai().toString());
        data.put("username",users.getUsername());
        dataInvoice.add(data);
        return dataInvoice;
    }

    private void servletResponse() {
        httpResponse.setContentType("application/pdf");
        httpResponse.addHeader("Content-Disposition", "inline; filename=kartu.pdf;");
    }

    private JasperReport giveJaspertReport() throws IOException,JRException{
        return JasperCompileManager.compileReport(ResourceUtils.getFile(ResourceUtils.CLASSPATH_URL_PREFIX
        +"invoice.jrxml").getAbsolutePath());
    }

    private HashMap<String,Object> giveParameter(){
        HashMap<String, Object> param = new HashMap<>();
        param.put("createdBy","lonelykatana");
        return param;
    }

    private JRBeanCollectionDataSource giveBeanCollection(String username, Character stdName, Integer nmrKursi, Long scheduleId){
        return new JRBeanCollectionDataSource(listDataForInvoice(username, stdName, nmrKursi, scheduleId));
    }

}

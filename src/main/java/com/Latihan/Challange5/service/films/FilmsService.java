package com.Latihan.Challange5.service.films;

import com.erick.challenge4.model.Films;


import java.util.List;
import java.util.Optional;

public interface FilmsService {

    void saveFilms(String filmName, Boolean isTayang);

    void updateFilmsInfoById(String filmName, Boolean isTayang, Long filmId);

    void deleteFilms(Long filmId);

    List<Films> findAllFilmsTayang();

    Films findFilmById(Long filmid);


}
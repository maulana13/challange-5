package com.erick.challenge4.service.schedules;

import com.erick.challenge4.model.Schedules;


import java.util.List;

public interface SchedulesService {

    void saveSchedule(String jamMulai, String jamSelesai, String tgl_tayang, Long filmId);
    List<Schedules> testFindFilmsByJadwal(Long filmId);

    Schedules findSchedulesById(Long schedulesId);
}

package com.Latihan.Challange5.service.schedules;

import com.erick.challenge4.model.Films;
import com.erick.challenge4.model.Schedules;
import com.erick.challenge4.repository.SchedulesRepository;
import com.erick.challenge4.service.films.FilmsService;
import com.erick.challenge4.service.schedules.SchedulesService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import static java.time.LocalDate.parse;

//sudah otomatis dibuat constructornya, dan kemudian dilakukan constructor injection secara otomatis oleh spring
@AllArgsConstructor
@Service
public class SchedulesServiceImpl implements SchedulesService {

    private SchedulesRepository schedulesRepository;

    private FilmsService filmsService;

    @Override
    public void saveSchedule(String jamMulai, String jamSelesai, String tglTayang, Long filmId) {
        Schedules schedules = new Schedules();
        schedules.setJamMulai(LocalTime.parse(jamMulai));
        schedules.setJamSelesai(LocalTime.parse(jamSelesai));
        schedules.setTglTayang(LocalDate.parse(tglTayang));
        Films films = filmsService.findFilmById(filmId);
        schedules.setFilmId(films);
        schedulesRepository.save(schedules);

    }


    @Override
    public List<Schedules> testFindFilmsByJadwal(Long filmId) {
        return schedulesRepository.testFindFilmsByJadwal(filmId);
    }

    @Override
    public Schedules findSchedulesById(Long schedulesId) {
        return schedulesRepository.findSchedulesById(schedulesId);
    }
}

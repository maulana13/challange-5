package com.erick.challenge4.service.users;

import com.erick.challenge4.model.Users;

public interface UsersService {

    void saveUsers(String username, String email, String password);
    void deleteUsersByUsername(String username);
    Users findUsersByUsername(String username);
}

package com.Latihan.Challange5.repository;

import com.erick.challenge4.model.Seats;
import com.erick.challenge4.model.SeatsId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SeatsRepository extends JpaRepository<Seats, SeatsId> {

}

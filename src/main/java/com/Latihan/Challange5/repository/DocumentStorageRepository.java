//package com.erick.challenge4.repository;
//
//import com.erick.challenge4.model.DocumentStorage;
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Query;
//
//public interface DocumentStorageRepository extends JpaRepository<DocumentStorage, Integer> {
//
//    @Query("Select a from DocumentStorage a where user_id = ?1 and document_type = ?2")
//    DocumentStorage checkDocumentByUserId(Integer userId, String docType);
//
//    @Query("Select fileName from DocumnentStorageProperties a where user_id = ?1 and document_type = ?2")
//    String getUploadDocumnetPath(Integer userId, String docType);
//
//}
//

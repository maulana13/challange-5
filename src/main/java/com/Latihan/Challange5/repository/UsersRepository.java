package com.Latihan.Challange5.repository;

import com.erick.challenge4.model.Films;
import com.erick.challenge4.model.Users;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public interface UsersRepository extends JpaRepository<Users, String> {

    @Modifying
    @Query(value = "delete from Users u where u.username=?1",nativeQuery = true)
    void deleteUsersByUsername(String username);

    @Query(value = "select * from Users u where u.username=?1", nativeQuery = true)
    Users findUsersByUsername(String username);
}


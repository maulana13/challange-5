package com.Latihan.Challange5.repository;

import com.erick.challenge4.model.Schedules;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SchedulesRepository extends JpaRepository<Schedules, Long> {


    @Query(value = "select sf.* from(select s.*,f.film_name from schedules s inner join films f on s.film_id=f.film_id)sf where sf.film_id=?1", nativeQuery = true)
    List<Schedules> testFindFilmsByJadwal(Long filmId);

    @Query(value = "select * from Schedules s where s.schedule_id=?1",nativeQuery = true)
    Schedules findSchedulesById(Long scheduleId);
}

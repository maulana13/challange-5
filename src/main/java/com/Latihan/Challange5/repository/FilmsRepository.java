package com.Latihan.Challange5.repository;

import com.erick.challenge4.model.Films;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface FilmsRepository extends JpaRepository<Films, Long> {

    @Modifying
    @Query(value = "update Films f set f.film_name=?1, f.is_tayang=?2 where f.film_id=?3",
            nativeQuery = true)
    void setFilmsInfoById(String filmName, Boolean isTayang, Long filmId);

    @Modifying
    @Query(value = "delete from Films f where f.film_id=?1", nativeQuery = true)
    void deleteFilmsbyId(Long filmId);


    @Query(value = "select * from Films f where f.is_tayang=true", nativeQuery = true)
    List<Films> findAllFilmsTayang();

    @Query(value = "select * from Films f where f.film_id=?1", nativeQuery = true)
    Films findFilmById(Long filmId);


}

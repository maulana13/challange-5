package com.Latihan.Challange5.controller;

import com.erick.challenge4.model.Films;
import com.erick.challenge4.model.Schedules;
import com.erick.challenge4.service.films.FilmsService;
import com.erick.challenge4.service.schedules.SchedulesService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

//sudah otomatis dibuat constructornya, dan kemudian dilakukan constructor injection secara otomatis oleh spring menggunakan autowired

@AllArgsConstructor
@RestController
@RequestMapping("/films")
public class FilmsController {
    private FilmsService filmsService;
    private SchedulesService schedulesService;

    //menambah film
    @PostMapping("/add-films")
    public String addFilms(@RequestBody Map<String, Object> film) {
        filmsService.saveFilms(film.get("filmName").toString(), (Boolean) film.get("isTayang"));
        return "Add Films Success!";
    }

    //mengupdate data di dalam table film
    @PutMapping("/update-films")
    public String updateFilmsInfoById(@RequestParam String filmName, @RequestParam Boolean isTayang, @RequestParam Long filmId) {
        filmsService.updateFilmsInfoById(filmName, isTayang, filmId);
        return "Update Film Success!";
    }

    //menghapus row dari table films berdasarkan film_id
    @DeleteMapping("/delete-films")
    public String deleteFilms(@RequestParam Long filmId) {
        filmsService.deleteFilms(filmId);
        return "Succes menghapus!";
    }

    //menampilkan semua film yang sedang tayang dari table films
    @GetMapping("/show-film-tayang")
    public ResponseEntity findAllFilmsTayang() {
        List<Films> films1 = filmsService.findAllFilmsTayang();
        //films1.forEach(films -> System.out.println(films.toString()));
        return new ResponseEntity(films1, HttpStatus.OK);
    }

    //menampilkan jadwal dari films yang sedang tayang
    @PostMapping("/films-by-jadwal")
    public ResponseEntity testFindFilmsByJadwal(@RequestParam Long filmId) {
        List<Schedules> schedules1 = schedulesService.testFindFilmsByJadwal(filmId);
        return new ResponseEntity(schedules1, HttpStatus.OK);
    }

    //menampilkan film dari table film berdasarkan film_id
    public void findFilmsById(Long filmId) {
        Films films = filmsService.findFilmById(filmId);
        System.out.println(films.getFilmId() + " " + films.getFilmName() + " " + films.getIsTayang());
    }
}

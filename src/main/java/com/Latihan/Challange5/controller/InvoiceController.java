package com.erick.challenge4.controller;

import com.erick.challenge4.service.invoice.InvoiceService;
import lombok.AllArgsConstructor;
import net.sf.jasperreports.engine.JRException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@AllArgsConstructor
@RestController
@RequestMapping("/invoice")
public class InvoiceController {

    private InvoiceService invoiceService;

    @GetMapping("/get-invoice")
    public ResponseEntity generateInvoice(@RequestParam String username, @RequestParam Character stdName, @RequestParam Integer nmrKursi, @RequestParam Long scheduleId)
            throws IOException, JRException {
        invoiceService.getInvoice(username, stdName, nmrKursi, scheduleId);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }
}

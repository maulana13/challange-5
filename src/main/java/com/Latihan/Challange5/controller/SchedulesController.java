package com.Latihan.Challange5.controller;

import com.erick.challenge4.model.Schedules;
import com.erick.challenge4.service.schedules.SchedulesService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//sudah otomatis dibuat constructornya, dan kemudian dilakukan constructor injection secara otomatis oleh spring menggunakan autowired

@AllArgsConstructor
@RestController
@RequestMapping("/schedules")
public class SchedulesController {
    private SchedulesService schedulesService;

    //menambahkan data ke dalam table schedule
    public void addSchedule(String jamMulai, String jamSelesai, String tglTayang, Long filmId) {
        schedulesService.saveSchedule(jamMulai, jamSelesai, tglTayang, filmId);
    }

    public void findSchedulesById(Long scheduleId){
        Schedules schedules = schedulesService.findSchedulesById(scheduleId);
        System.out.println(schedules.getScheduleId()+ " "+ schedules.getJamMulai()+ " "+schedules.getJamSelesai()
        +" "+schedules.getTglTayang()+" "+ schedules.getFilmId().getFilmId());
    }
}

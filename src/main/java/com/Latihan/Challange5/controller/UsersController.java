package com.Latihan.Challange5.controller;


import com.erick.challenge4.model.Users;
import com.erick.challenge4.service.users.UsersService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Tag(name = "Users", description = "API for processing Users entity")
//sudah otomatis dibuat constructornya, dan kemudian dilakukan constructor injection secara otomatis oleh spring menggunakan autowired
@AllArgsConstructor
@RestController
@RequestMapping("/users")
public class UsersController {
    private UsersService usersService;

    @Operation(summary = "add films to Films table in Challenge4 Database")
    //menambahkan data ke dalam table users..
    @PostMapping("/add-users")
    public String addUsers(@Schema(example = "{" +
            "\"username\":\"Erick\"," +
            "\"email\":\"erick@gmail.com\"," +
            "\"password\":\"123\"," +
            "}") @RequestBody Map<String, Object> user) {
        usersService.saveUsers(user.get("username").toString(), user.get("email").toString(),
                user.get("password").toString());
        return "Add Users Success!";
    }

    // ..bisa juga sebagai update, jika username yang dimasukkan sudah ada sebelumnya
    @PutMapping("/update-users")
    public String updateUsers(@RequestBody Map<String, Object> user) {
        usersService.saveUsers(user.get("username").toString(), user.get("email").toString(),
                user.get("password").toString());
        return "Update Users Success!";
    }

    //menghapus row dari table users berdasarkan username
    @DeleteMapping("/delete-users")
    public void deleteUsersByUsername(@RequestParam String username) {
        usersService.deleteUsersByUsername(username);
    }

    public void findUsersByUsername(String username) {
        Users users = usersService.findUsersByUsername(username);
        System.out.println(users.getUsername() + " " + users.getEmail() + " " + users.getPassword());
    }
}

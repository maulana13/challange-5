package com.Latihan.Challange5.controller;

import com.erick.challenge4.service.seats.SeatsService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//sudah otomatis dibuat constructornya, dan kemudian dilakukan constructor injection secara otomatis oleh spring menggunakan autowired

@AllArgsConstructor
@RestController
@RequestMapping("/seats")
public class SeatsController {
    private SeatsService seatsService;

    public void addSeats(Character stdName, Integer nmrKursi, Long scheduleId){
        seatsService.addSeats(stdName, nmrKursi, scheduleId);
    }
}

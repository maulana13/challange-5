//package com.erick.challenge4.controller;
//
//import com.erick.challenge4.model.Films;
//import com.erick.challenge4.model.Schedules;
//import com.erick.challenge4.service.ApiClient;
//import com.erick.challenge4.service.films.FilmsService;
//import com.erick.challenge4.service.schedules.SchedulesService;
//import com.erick.challenge4.service.users.UsersService;
//import lombok.AllArgsConstructor;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.client.RestTemplate;
//
//import java.util.List;
//
////sudah otomatis dibuat constructornya, dan kemudian dilakukan constructor injection secara otomatis oleh spring
//@AllArgsConstructor
//@RestController
//@RequestMapping("/app")
//public class AppController {
//
//    private FilmsService filmsService;
//    private SchedulesService schedulesService;
//    private UsersService usersService;
//
//
//
//    //menambah data ke dalam table films
//
//    public void addFilms(String filmName, Boolean isTayang) {
//        filmsService.saveFilms(filmName, isTayang);
//    }
//
//    //mengupdate data di dalam table film
//    public void updateFilmsInfoById(String filmName, Boolean isTayang, Long filmId) {
//        filmsService.updateFilmsInfoById(filmName, isTayang, filmId);
//    }
//
//    //menghapus row dari table films berdasarkan film_id
//    public void deleteFilms(Long filmId) {
//        filmsService.deleteFilms(filmId);
//    }
//
//    //menampilkan semua film yang sedang tayang dari table films
//    public void findAllFilmsTayang() {
//        List<Films> films1 = filmsService.findAllFilmsTayang();
//        films1.forEach(films -> System.out.println(films.toString()));
//    }
//
//    //menambahkan data ke dalam table schedule
//    public void addSchedule(String jamMulai, String jamSelesai, String tglTayang, Long filmId) {
//        schedulesService.saveSchedule(jamMulai, jamSelesai, tglTayang, filmId);
//    }
//
//    //menampilkan jadwal dari films yang sedang tayang
//    public void testFindFilmsByJadwal(Long filmId) {
//        List<Schedules> schedules1 = schedulesService.testFindFilmsByJadwal(filmId);
//        System.out.println("film_id \tfilm_name \t\ttgl_tayang \t\tjam_mulai \t\tjam_selesai ");
//        for (Schedules schedules : schedules1) {
//            System.out.println(schedules.getFilms().getFilmId() + " \t" + schedules.getFilms().getFilmName() + " \t" + schedules.getTglTayang() + " \t\t" + schedules.getJamMulai() + " \t\t\t"
//                    + schedules.getJamSelesai());
//        }
//    }
//
//    //menambahkan data ke dalam table users
//    //bisa juga sebagai update, jika username yang dimasukkan sudah ada seblumnya
//    public void addUsers(String username, String email, String password) {
//        usersService.saveUsers(username, email, password);
//    }
//
//    //menghapus row dari table users berdasarkan username
//    public void deleteUsersByUsername(String username) {
//        usersService.deleteUsersByUsername(username);
//    }
//
//
//    //menampilkan film dari table film berdasarkan film_id
//    public void findFilmsById(Long filmId) {
//        Films films = filmsService.findFilmById(filmId);
//        System.out.println(films.getFilmId() + " " + films.getFilmName() + " " + films.getIsTayang());
//    }
//
//
//
//
//}
//
//
//
//
//

package com.erick.challenge4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@SpringBootApplication
@ConfigurationPropertiesScan
public class Challenge4Application {

    public static void main(String[] args) {
        SpringApplication.run(Challenge4Application.class, args);
        System.out.println("Hello World!");
    }

}

package com.Latihan.Challange5.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

import static javax.persistence.CascadeType.ALL;

@Entity
@Getter
@Setter
@Table(name = "films")
public class Films implements Serializable {

    @Id
    @GeneratedValue
    private Long filmId;

    @Column(name = "film_name")
    private String filmName;

    @Column(name = "is_tayang")
    private Boolean isTayang;

    @Override
    public String toString() {
        return "films [  film_id : " + filmId + "" +
                "\n\t\t film_name : " + filmName + " \n\t\t is_tayang : " + isTayang + " ]";
    }

}

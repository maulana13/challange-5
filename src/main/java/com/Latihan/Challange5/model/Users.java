package com.Latihan.Challange5.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Getter
@Setter
@Table(name="users")

public class Users implements Serializable {

    @Id
    @Column(name = "username",nullable = false,unique = true)
    private String username;

    @Column(name="email")
    private String email;

    @Column(name = "password")
    private String password;
}

package com.Latihan.Challange5.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;


@Entity
@Getter
@Setter
@Table(name = "schedules")
public class Schedules implements Serializable {

    @Id
    @GeneratedValue
    private Long scheduleId;

    @Column(name = "tgl_tayang")
    private LocalDate tglTayang;

    @Column(name = "jam_mulai")
    private LocalTime jamMulai;

    @Column(name = "jam_selesai")
    private LocalTime jamSelesai;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "film_id")
    private Films filmId;

    @Override
    public String toString() {
        return "Schedules{" +
                "scheduleId=" + scheduleId +
                ", tglTayang=" + tglTayang +
                ", jamMulai=" + jamMulai +
                ", jamSelesai=" + jamSelesai +
                ", films=" + filmId +
                '}';
    }
}

package com.Latihan.Challange5.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor

public class SeatsId implements Serializable {

    @Column(length = 1)
    private Character stdName;

    @Column(length = 3)
    private Integer nmrKursi;



}

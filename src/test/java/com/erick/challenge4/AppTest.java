package com.erick.challenge4;


import com.erick.challenge4.controller.FilmsController;
import com.erick.challenge4.controller.SchedulesController;
import com.erick.challenge4.controller.SeatsController;
import com.erick.challenge4.controller.UsersController;
import com.erick.challenge4.model.Schedules;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
public class AppTest {
    @Autowired
    private UsersController usersController;
    @Autowired
    private FilmsController filmsController;
    @Autowired
    private SchedulesController schedulesController;
    @Autowired
    private SeatsController seatsController;

    //menambah data ke dalam table films
//    @Test
//    @DisplayName("Test ADD Films")
////    public void addFilms() {
////        filmsController.addFilms("Evanglion 3.0", true);
////    }

    //mengupdate data di dalam table film
//    @Test
//    @DisplayName("Test UPDATE Films")
//    public void updateFilmsById() {
//        filmsController.updateFilmsInfoById("Evangelion 3.0 + 1.0", true, 11L);
//    }

    //menghapus row dari table films berdasarkan film_id
    @Test
    @DisplayName("Test DELETE Films")
    public void deleteFilms() {
        filmsController.deleteFilms(2L);
    }

    //menampilkan jadwal dari films yang sedang tayang
    @Test
    @DisplayName("Test Get Films yang Tayang")
    public void filmsTayang() {
        filmsController.findAllFilmsTayang();
    }

    //menambahkan data ke dalam table schedule
    @Test
    @DisplayName("Test ADD Schedule")
    public void addSchedule() {
        schedulesController.addSchedule("21:00", "22:00", "2022-04-21", 11L);
    }

    //menampilkan jadwal dari films yang sedang tayang
    @Test
    @DisplayName("Test filmByJadwal")
    public void testFindFilmsByJadwal() {
        filmsController.testFindFilmsByJadwal(7L);
    }

    //menambahkan data ke dalam table users
    //bisa juga sebagai update Users jika username yg dimasukkan sudah ada sebelumnya
//    @Test
//    @DisplayName("Test ADD/UPDATE Users")
//    public void addUsers() {
//        usersController.addUsers("gessel", "gessel@mail.com", "598");
//    }

    //menghapus row dari table users berdasarkan username
    @Test
    @DisplayName("Test DELETE Users")
    public void deleteUsersByUsername() {
        usersController.deleteUsersByUsername("ruben");
    }

    ////menampilkan film dari table film berdasarkan film_id
    @Test
    @DisplayName("Test FindById")
    public void findByFilmId() {
        filmsController.findFilmsById(4L);
    }

    @Test
    @DisplayName("Test ADD Seats")
    public void addSeats() {
        seatsController.addSeats('A', 21, 10L);
    }

    @Test
    @DisplayName("Test findUsersByUsername")
    public void findUsersByUsername() {
        usersController.findUsersByUsername("erick");
    }


}
